console.log('hellow');

fetch('https://jsonplaceholder.typicode.com/todos')
	.then((response) => response.json())
	.then((data) => {
		dataMap = data.map(function(data){
			return data.title
		})
		console.log(dataMap)
	})


fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response) => response.json())
	.then((data) => {
	console.log(`The item "${data.title}" on the list has a status of ${data.completed}`)
		})


fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
		body: JSON.stringify({
		completed : false,
		title: 'Created To Do List Item',
		userId: 1
		})
	})
	.then((response) => response.json())
	.then((data) => console.log(data));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		headers: {
		'Content-Type': 'application/json'
	},
		body: JSON.stringify({
		title: 'Updated to do list item',
		description : "to update the my to do list with a different data structure",
		status : 'Pending',
		dateCompleted : 'Pending',
		userId : 1
	})
})
		.then((response) => response.json())
		.then((data) => console.log(data));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PATCH',
		headers: {
		'Content-Type': 'application/json'
	},
		body: JSON.stringify({
		status : 'Complete',
		dateCompleted : '02/23/2023'

	})
})
		.then((response) => response.json())
		.then((data) => console.log(data));